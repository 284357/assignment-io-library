section .text
 
 
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
        xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rax
	push rdi
	mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall
    pop rdi
    pop rax
    ret


; Принимает код символа и выводит его в stdout
print_char:
	push rsp
    push rdi
	mov rax, 1
	mov rdi, 1
	mov rsi, rsp
	mov rdx, 1
	syscall
    pop rdi
    pop rsp
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	push rdi
	
	mov rdi, 0xA
	call print_char
	
	pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    push r9

    mov r9, rsp
    mov rax, rdi
    mov rbx, 10
    push 0
    .divide:
        xor rdx, rdx
        div rbx
        add rdx, '0'
        dec rsp
        mov byte [rsp], dl
        test rax, rax
        jnz .divide
    mov rdi, rsp
    push rdi
    push rdx
    push rax
    call print_string
    pop rax
    pop rdx
    pop rdi
    mov rsp, r9
	
    pop r9
    pop rbx
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:

    cmp rdi, 0
    jge print_uint
    push rdi
	
    mov rdi, '-'
    call print_char
	
    pop rdi
    neg rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки (rdi, rsi), возвращает 1 если они равны, 0 иначе
string_equals:
    push rsi
	
    call string_length 
    mov r9, rax
    mov r11, rdi
    pop rdi
    call string_length
    cmp r9, rax
    jne .not_equal
    .loop:
        mov al, byte [rdi]
        cmp al, byte [r11]
        jne .not_equal
        cmp byte [rdi], 0
        je .end
        inc rdi
        inc r11
        jmp .loop
    .not_equal:
        mov rax, 0
        ret
    .end:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
	
    mov rdi, 0
    mov rdx, 1
    mov rax, 0
    push 0
    mov rsi, rsp
    syscall
	
    pop rax
    pop rdi
    ret 

; Принимает: адрес начала буфера (rdi), размер буфера (rsi)
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
        xor r9, r9
        push rdi
        push r13
		
        mov r13, rsi
        dec r13
        .start:
            call read_char
            cmp al, 0x20
            je .start
            cmp al, 0x9
            je .start
            cmp al, 0xA
            je .start
            cmp al, 0
            je .end
        .write:
            inc r9
            mov byte [rdi], al
            inc rdi
            cmp r9, r13
            je .overflow
            call read_char
            cmp al, 0x20
            je .end
            cmp al, 0x9
            je .end
            cmp al, 0xA
            je .end
            cmp al, 0
            je .end
            jmp .write
        .overflow:
            pop r13
            pop rdi
            xor rax, rax
            ret
        .end:
            mov byte [rdi], 0
			
            pop r13
            pop rax
            mov rdx, r9
            ret
    
; Принимает указатель на строку (rdi), пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        mov r9, -1
        xor r10, r10
        xor rax, rax
    .init_loop:
        cmp byte [rdi], '0'
        jb .error
        cmp byte [rdi], '9'
        ja .error
    .loop_a:
        cmp byte [rdi], '0'
        jb .end
        cmp byte [rdi], '9'
        ja .end
        cmp byte [rdi], 0
        je .end
        inc r9
        mov r10b, byte [rdi]
        sub r10, '0'
        inc rdi
        cmp r9, 0
        ja .mul
        mov rax, r10
        jmp .loop_a
    .mul:
        imul rax, 10
        add rax, r10
        jmp .loop_a
    .error:
        mov rdx, 0
        ret
    .end:
        inc r9
        mov rdx, r9
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    je .int
    call parse_uint
    ret
    .int:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
    ret 

; Принимает указатель на строку (rdi), указатель на буфер (rsi) и длину буфера (rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rsi
    push rdi
	
    xor r9, r9
    xor rax, rax
    call string_length 
    pop rdi
    pop rsi
    cmp rax, rdx
    ja .overflow
    .copy:
        mov r9b, byte[rdi]
        mov byte[rsi], r9b
        inc rsi
        inc rdi
        cmp r9b, 0
        je .exit
        jmp .copy
    .exit:
        ret
    .overflow:
        mov rax, 0
        ret
